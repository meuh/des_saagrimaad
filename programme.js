let nbr_succes,nbr_avtg,nbr_ripst;
let texte_affiche = "";
let txt_dernier_jet = "";
let nbr_aleat;
let page_resultats = document.getElementById("contenant_historique");
let noeud_dern_res = document.getElementById('derniers_resultats');
let nbr_des;

function lien_boutons(nom){
	//Boite de dialogue
	let id_boite = "Nbr_"+nom;
	let boite_nbr = document.getElementById(id_boite);
	//Curseur
	let id_curs = "Curs_" + nom;
	let curseur = document.getElementById(id_curs);
	//Bouton +
	let id_plus = "Plus_" + nom;
	let btn_plus = document.getElementById(id_plus);
	//Bouton -
	let id_moins = "Moins_" + nom;
	let btn_moins = document.getElementById(id_moins);

	//Boite de dialogue
	boite_nbr.oninput = function(){
		curseur.value = this.value;
	}
	//Curseur
	curseur.oninput = function(){
		boite_nbr.value = this.value;
	}
	//Bouton +
	btn_plus.onclick = function(){
		valeur = parseInt(boite_nbr.value);
		nvelle_valeur = valeur + 1;
		boite_nbr.value = nvelle_valeur;
		curseur.value = nvelle_valeur;
	}
	//Bouton -
	btn_moins.onclick = function(){
		valeur = parseInt(boite_nbr.value);
		nvelle_valeur = valeur - 1;
		boite_nbr.value = nvelle_valeur;
		curseur.value = nvelle_valeur;
	}
}

function interface_entrees(){
	nom_des.forEach(lien_boutons);
}
interface_entrees();

function lancer(nom){
		let id = "Nbr_"+nom;
		nbr_des = parseInt(document.getElementById(id).value);
		for(i=0;i<nbr_des;i++){
			//Un jet entre 0 et 5
			nbr_aleat = Math.floor(Math.random() * table_corresp[nom].length);
			//On sélectione le résultat du dé corrspondant dans le tableau
			let res_jet = table_corresp[nom][nbr_aleat];
			nbr_succes += res_jet[0];
			nbr_avtg += res_jet[1];
			nbr_ripst += res_jet[2];
			texte_affiche += res_jet[3];
			if(i<nbr_des-1) texte_affiche += " , ";
			else texte_affiche += "   ;   ";
		}
}

function lancer_des(){
	nbr_succes = 0;
	nbr_avtg   = 0;
	nbr_ripst   = 0;
	texte_affiche = "";

	//On lance tous les dés
	nom_des.forEach(lancer);

	//On modifie les résultats du jet en fonction du dé d'avantages
	if(nbr_avtg > 1) nbr_succes += nbr_avtg - 1;
	if(nbr_avtg < -1) nbr_succes += nbr_avtg -(-1);

	//On affiche soit 'bonus' ou 'malus' selon le nombre d'avantages
	let mot_avtg;
	if(nbr_avtg > 0) mot_avtg = "Bonus";
	else if(nbr_avtg < 0) mot_avtg = "Malus";
	else mot_avtg = "Rien"

	//Affichage du dernier jet
	txt_dernier_jet = "Succès : " + String(nbr_succes) + " ; Bonus/Malus : " + mot_avtg + " ; Ripostes : " + String(nbr_ripst);
	noeud_dern_res.innerText = txt_dernier_jet;

	//Création d'un historique des derniers jets
	p_hist = document.createElement("p");
	p_hist.innerText = texte_affiche;
	//On ajoute la dernière entrée au début de la liste
	page_resultats.insertBefore(p_hist, page_resultats.firstChild);
}

function effacer_historique(){
	while(page_resultats.hasChildNodes()){
		page_resultats.removeChild(page_resultats.childNodes[0]);
	}
	
}

