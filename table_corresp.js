
var table_corresp = [];
var nom_des = ["Co","Av","Des","Ri","Ch"];
var faces_des = [];
/*
Ce tableau de correspondance permet d'obtenir les valeurs sur les dés particuliers à partir
de deux valeurs numériques : le type de dé et le résultat. Pour chaque correspondance, il possède
4 valeurs distinctes :
	-Le nombre de '+'' (positif) ou de 'X' (négatif)
	-Le nombre de '◯' (positif) ou de '◇' (négatif)
	-Le nombre de '↪︎'
	-Si le symbole est '·' => on met zéro
	-Caractères pour l'affichage
*/
table_corresp["Co"] = [
[1,0,0,"+"],
[1,0,0,"+"],
[1,0,0,"+"],
[0,1,0,"◯"],
[0,0,0,"·"],
[0,0,0,"·"],
];

table_corresp["Av"] = [
[1,0,0,"+"],
[1,1,0,"◯+"],
[0,1,0,"◯"],
[0,0,0,"·"],
];

table_corresp["Des"] = [
[-1,0,0,"X"],
[-1,-1,0,"X◇"],
[0,-1,0,"◇"],
[0,0,0,"·"],
];

table_corresp["Ri"] = [
[-1,0,0,"X"],
[-1,0,0,"X"],
[0,-1,0,"◇"],
[0,-1,0,"◇"],
[0,0,1,"↪︎"],
[0,0,1,"↪︎"],
[0,0,1,"↪︎"],
[0,0,1,"↪︎"]
]

table_corresp["Ch"] = [
[-1,0,0,"X"],
[-1,1,0,"◯X"],
[-1,-1,0,"◇X"],
[0,0,0,"·"],
[0,1,0,"◯"],
[0,-1,0,"◇"],
[-1,0,0,"X"],
[-1,0,0,"X"],
[1,0,0,"+"],
[1,1,0,"◯+"],
[0,-1,0,"◇"],
[2,0,0,"++"],
[2,1,0,"◯++"],
[1,0,0,"+"],
[1,0,0,"+"],
[2,0,0,"++"],
[2,0,0,"++"],
[2,-1,0,"◇++"],
[0,0,0,"·"],
[0,0,0,"·"],
]

